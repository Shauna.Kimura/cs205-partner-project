def is_sold(car):
    return car.is_sold


class Car:
    def __init__(self, manufacturer, model, color, year, msrp, vin):
        self.manufacturer = manufacturer
        self.model = model
        self.color = color
        self.year = year
        self.msrp = msrp
        self.vin = vin
        self.in_stock = True

    def __str__(self):
        return str(self.year) + ' ' + str(self.manufacturer) + ' ' + str(self.model) + '\nExt. Color: ' + \
               self.color + '\nMSRP: $' + '{:,}'.format(self.msrp) + '\nVIN: ' + self.vin + '\nIn Stock: ' + \
               str(self.in_stock) + '\n'

    def __eq__(self, car):
        return self.vin == car.vin

    def __hash__(self):
        return hash((self.manufacturer, self.model, self.color, self.year, self.msrp, self.vin))

    # getters
    def get_manufacturer(self):
        return self.manufacturer

    def get_model(self):
        return self.model

    def get_color(self):
        return self.color

    def get_year(self):
        return self.year

    def get_msrp(self):
        return self.msrp

    def get_vin(self):
        return self.vin
