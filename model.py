class Model:
    def __init__(self, *args):
        # base constructor
        if len(args) == 1:
            self.name = args[0].name
            self.manufacturer = args[0].manufacturer
        # alternate constructor
        elif len(args) == 2:
            self.name = args[0]
            self.manufacturer = args[1]
        else:
            print('Incorrect number of arguments.')

    def __str__(self):
        return self.name

    # getters
    def get_model_name(self):
        return self.name

    def get_manufacturer(self):
        return self.manufacturer
