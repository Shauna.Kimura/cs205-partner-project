class Dealership:
    s_dealership = None

    @classmethod
    def get(cls):
        if cls.s_dealership is None:
            cls.s_dealership = Dealership()
        return cls.s_dealership

    def __init__(self):
        self.stocked_cars = set()
        self.sold_cars = set()

    def add_car(self, car):
        self.stocked_cars.add(car)

    def sell_car(self, car):
        for c in self.stocked_cars:
            if c == car:
                self.sold_cars.add(car)
                car.in_stock = False

        self.stocked_cars.remove(car)

    def incorrect_sell_car(self, car):
        for c in self.stocked_cars:
            if c == car:
                self.sold_cars.add(car)
                car.in_stock = False

    def show_sold_cars(self):
        for c in self.sold_cars:
            print(c)

    def show_stocked_cars(self):
        for c in self.stocked_cars:
            print(c)

    def get_stocked_cars(self):
        return self.stocked_cars

    def get_sold_cars(self):
        return self.sold_cars
