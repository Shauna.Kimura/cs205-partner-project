import unittest
import dealership
from car import Car


class TestSellCar(unittest.TestCase):
    library = None

    @classmethod
    def setUpClass(cls):
        print('setUpClass()')
        cls.dealership = dealership.Dealership().get()

        # create some cars
        manufacturer_a = 'Toyota'
        manufacturer_b = 'Honda'
        manufacturer_c = 'Ford'
        manufacturer_d = 'Tesla'

        model_a = 'Camry'
        model_b = 'Civic'
        model_c = 'Explorer'
        model_d = 'Model S'

        cls.car_1 = Car(manufacturer_a, model_a, 'Red', 2020, 25965, '4T1G11AK3LU339300')
        cls.car_2 = Car(manufacturer_b, model_b, 'Grey', 2018, 16998, 'SHHFK7H27JU428698')
        cls.car_3 = Car(manufacturer_c, model_c, 'White', 2021, 42605, '1FMSK8DH3MGA35917')
        cls.car_4 = Car(manufacturer_d, model_d, 'Silver', 2017, 59999, '5YJSA1E25HF185973')
        cls.car_5 = Car(manufacturer_d, model_d, 'Silver', 2017, 59999, '5YJSA1E25HF185973')
        cls.car_6 = Car(manufacturer_d, model_d, 'Silver', 2017, 59999, '5YJ3E1EB2KF389940')

        cls.dealership.add_car(cls.car_1)
        cls.dealership.add_car(cls.car_2)
        cls.dealership.add_car(cls.car_3)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass()')

    def setUp(self):
        print('setUp()')

    def tearDown(self):
        print('tearDown()')
        # restart everything

    # dealership - add car and check that stocked cars now contains that car
    def test_add_car_one(self):
        # add car
        self.dealership.add_car(self.car_4)

        # get the new stock of cars
        stocked_cars = self.dealership.get_stocked_cars()

        # check to see if car is now in stock
        self.assertTrue(self.car_4 in stocked_cars)

    # sell car and check that the sold car is not contained in stock
    def test_sell_car_one(self):
        # sell a car
        self.dealership.sell_car(self.car_4)

        # get the new stock of cars
        stocked_cars = self.dealership.get_stocked_cars()

        # check to see if car sold is not in stock
        self.assertTrue(self.car_4 not in stocked_cars)

    # incorrect - sell car and check that the sold car is not contained in stock
    def test_sell_car_one_incorrect(self):
        # sell a car
        self.dealership.incorrect_sell_car(self.car_2)

        # get the new stock of cars
        stocked_cars = self.dealership.get_stocked_cars()

        # check to see if car sold is not in stock
        self.assertTrue(self.car_2 not in stocked_cars)

    # dealership - sell car and check that sold cars now contains that car
    def test_sell_car_two(self):
        # sell a car
        self.dealership.sell_car(self.car_1)

        # get cars sold
        sold_cars = self.dealership.get_sold_cars()

        # check to see if car_1 is contained in sold cars
        self.assertTrue(self.car_1 in sold_cars)

    # check __eq__ in cars, add two cars that are the same and have the same vin, and see if they are equal
    def test_eq_one(self):
        self.assertEqual(self.car_4, self.car_5)

    # check __eq__ in cars, add two cars that are the same but have different vins, and see if they are not equal
    def test_eq_two(self):
        self.assertNotEqual(self.car_5, self.car_6)


if __name__ == "__main__":
    unittest.main()
