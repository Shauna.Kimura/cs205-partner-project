class Manufacturer:
    def __init__(self, *args):
        # base constructor
        if len(args) == 1:
            self.brand = args[0].brand
            self.country = args[0].country
        # alternate constructor
        elif len(args) == 2:
            self.brand = args[0]
            self.country = args[1]
        else:
            print('Incorrect number of arguments.')

    def __str__(self):
        return self.brand

    def get_brand(self):
        return self.brand

    def get_country(self):
        return self.country
