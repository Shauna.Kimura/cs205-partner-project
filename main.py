# Shauna Kimura
# Scotti Day
# CS 205 Partner Project

from car import Car
from dealership import Dealership
from manufacturer import Manufacturer
from model import Model


def simple_tests(dealership):
    manufacturer_1 = Manufacturer("Volkswagen", 'Germany')
    model_1 = Model("Jetta", manufacturer_1)
    model_2 = Model("Tiguan", manufacturer_1)

    car_1 = Car(manufacturer_1, model_1, 'Black', 2019, 20000, 'SK00YJ24L99BW1134')
    car_2 = Car(manufacturer_1, model_2, 'Blue', 2017, 18000, 'CS99RM188UE44JK03')

    dealership.add_car(car_1)
    dealership.add_car(car_2)
    print('Stocked Cars: ')
    print('------------------------------')
    dealership.show_stocked_cars()

    print('Selling cars...')
    print()

    dealership.sell_car(car_1)
    dealership.sell_car(car_2)

    print('Sold Cars: ')
    print('------------------------------')
    dealership.show_sold_cars()


def main():
    dealership = Dealership()
    simple_tests(dealership)


main()
